#define N 9

chan in[N] = [0] of {byte}
chan out[N]= [0] of {byte}

proctype Train(int id){
	do
		::  
		printf("Pociag %d czeka\n", id);
		in[id]!true;
		printf("Pociag %d wjechal\n", id);
        out[id]!false;
        printf("Pociag %d wyjechal\n", id);
	od
}

proctype Semafor(){
    int i;
    do
        :: select (i : 0 .. N-1);
            in[i]?true; out[i]?false;
    od
}

init {
    int i;
	atomic {
        for (i : 0 .. N-1) {
            run Train(i);
        }
		run Semafor();
	}
}
