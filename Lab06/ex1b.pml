#define N	10
init {	/* file: ex.1b */
	chan dummy = [N] of { byte };
	do
	:: dummy!85
	:: dummy!170
	od
}
