#define N = 3;

chan in1 = [0] of {int};
chan in2 = [0] of {int};
chan in3 = [0] of {int};
chan out1 = [0] of {int};
chan out2 = [0] of {int};
chan out3 = [0] of {int};

proctype Train1(int train){
	do
		::  
		printf("Pociag %d czeka\n", train);
		in1!true;
		printf("Pociag %d wjechal\n", train);
		atomic {
			out1!false;
			printf("Pociag %d wyjechal\n", train);
		}
	od
}
proctype Train2(int train){
	do
		::  
		printf("Pociag %d czeka\n", train);
		in2!true;
		printf("Pociag %d wjechal\n", train);
		atomic {
			out2!false;
			printf("Pociag %d wyjechal\n", train);
		}
	od
}
proctype Train3(int train){
	do
		::  
		printf("Pociag %d czeka\n", train);
		in3!true;
		printf("Pociag %d wjechal\n", train);
		atomic {
			out3!false;
			printf("Pociag %d wyjechal\n", train);
		}
	od
}


proctype Semafor(){
do
::in1?true; out1?false;
::in2?true; out2?false;
::in3?true; out3?false;
od
}

init {
	atomic {
		run Train1(1);
		run Train2(2);
		run Train3(3);
		run Semafor();
	}
}
