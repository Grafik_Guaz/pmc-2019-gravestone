#define N 5
byte forks[N] = 0;
int count = 0
proctype DinningRoom(chan in,out){
 byte id
 do
 :: count < N - 1 ->
    atomic{
          in?id
          printf("Philosopher enter: %d\n", id)
          count++
          printf("Cunter %d\n",count)
    }
 :: count <= N - 1 && count > 0 ->
    atomic{
          out?id
          printf("Philosopher left: %d\n", id)
          count--
          printf("Cunter %d\n",count)
    }
 od
}
proctype Philosopher(chan in,out; byte left,right,id){
  do
    :: true ->
      in!id
      atomic{
        if
        ::(forks[left]==0) ->
          forks[left]=1;
          printf(" %d waiting\n",id);
        fi
      }
      atomic{
        if
        ::(forks[left]==1 && forks[right]==0) ->
          forks[right]=1;
          printf(" %d eating\n",id);
        fi
      }
      atomic{
        if
        ::(forks[left]==1 && forks[right]==1) ->
          forks[right]=0;
          forks[left]=0;
          printf(" %d thinking\n",id);
        fi
      }
      out!id
  od
}

init{
    chan in = [0] of {byte};
    chan out = [0] of {byte};
    run DinningRoom(in, out);
    run Philosopher(in, out,N - 1,0,0);
    int counter = 1;
    do
    :: (counter < N) ->
      run Philosopher(in, out, counter-1,counter,counter);
      counter++;
    :: else -> break;
    od
}

ltl mutex {[](count <= N-1 && count > =0)}
