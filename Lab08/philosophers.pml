#define N 15

byte forks[N] = 0;
int counter = 0;

proctype Philosopher(byte left,right,id)
{
 byte less = (left<right -> left : right);
 byte greater = (left<right -> right : left);

 do
   :: true ->
     atomic
     {
       if
       ::(forks[less]==0) ->
         forks[less]=1;
         printf(" %d waiting\n",id);
       fi
     }
     atomic
     {
       if
       ::(forks[less]==1 && forks[greater]==0) ->
         forks[greater]=1;
         counter++;
         printf(" %d eating\n",id);
       fi
     }
     atomic
     {
       if
       ::(forks[less]==1 && forks[greater]==1) ->
         forks[greater]=0;
         forks[less]=0;
         counter--;
         printf(" %d thinking\n",id);
       fi
     }
 od
}

init
{
    run Philosopher(N - 1,0,0);
    int count = 1;
    do
    :: (count < N) ->
      run Philosopher(count-1,count,count);
      count++;
    :: else -> break;
    od
    /*
    atomic
    {
    run Philosopher(4,0,0);
    run Philosopher(0,1,1);
    run Philosopher(1,2,2);
    run Philosopher(2,3,3);
    run Philosopher(3,4,4);
    }
    */
}

ltl mutex {[](counter <=  N-1 && counter >= 0)}
