#define N 5

chan forks[N] = [0] of {byte};
proctype Philosopher(byte id; chan left,right)
{
  do
  :: true  -> atomic
    {
      left!id;
      right!id;
      printf("filozof %d je\n",id);
      left?id;
      printf("odłożyłem pierwszy")
      right?id;
      printf("odłożyłem drugi")
    }
  od
}

proctype Fork(chan fork)
{
  byte phil;
  do
  :: true -> atomic
  {
      fork?phil;
      printf("Podniósł mnie filozof %d\n",phil);
      fork!phil;
  }
  od
}

init
{
    run Fork(forks[0]);
    run Fork(forks[1]);
    run Fork(forks[2]);
    run Fork(forks[3]);
    run Fork(forks[4]);

    run Philosopher(0,forks[0],forks[1]);
    run Philosopher(1,forks[1],forks[2]);
    run Philosopher(2,forks[2],forks[3]);
    run Philosopher(3,forks[3],forks[4]);
    run Philosopher(4,forks[4],forks[0]);
}
