chan store = [false] of {bool};
chan prt_cmd = [false] of {bool};
chan print = [false] of {bool};

proctype scanner() {
  int n = 0;
  do
  :: n++; printf("Skanuje %d \n", n); store!true;
  od
}

proctype program() {
  int n = 0;
  do 
  :: store?true; n++; printf("Przekazuje %d \n", n); prt_cmd!true; 
  od
}

proctype printer() {
  int n = 0;
  do 
  :: prt_cmd?true; n++; printf("Drukuje %d \n", n); print!true;
  od
}
 

init {
  atomic {
    run scanner()
    run program()
    run printer()
  }
}

