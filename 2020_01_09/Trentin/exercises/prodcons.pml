/**
 * Producer / Consumer
 *
 * author: unknown
 *
 */

mtype = { P, C, N };

mtype turn = P;
pid   who;

inline request(x, y, z) {
  atomic { x == y -> x = z; who = _pid }
}

inline release(x, y) {
  atomic { x = y; who = 0 }
}

active [2] proctype producer()
{
  do
    :: request(turn, P, N) ->
production:
       printf("Produce %d\n", _pid);
       assert(who == _pid);
       release(turn, C)
  od
}

active [2] proctype consumer()
{
  do
    :: request(turn, C, N) ->
consumption:
       printf("Consume %d\n", _pid);
       assert(who == _pid);
       release(turn, P)
  od
}

/* production and consumptions alternate */
ltl p1 { [](
            (producer[0]@production -> (!producer[1]@production U (consumer[2]@consumption || consumer[3]@consumption)))
         && (producer[1]@production -> (!producer[0]@production U (consumer[2]@consumption || consumer[3]@consumption)))
         && (consumer[2]@consumption -> (!consumer[3]@consumption U (producer[0]@production || producer[1]@production)))
         && (consumer[3]@consumption -> (!consumer[2]@consumption U (producer[0]@production || producer[1]@production)))
  )
}
