/**
 * Alternating Bit Protocol (fixed)
 *
 * author: unknown
 *
 */

mtype = { MESSAGE_A, MESSAGE_B, ACK };

chan sender2receiver = [2] of { mtype, bit};
chan receiver2sender = [2] of { mtype, bit};

inline unreliable_send(channel, type, tag) {
    if
        :: channel!type(tag) ->
            loss = false;
        :: loss = true;
    fi
}

active proctype Sender () {
    bit bit_in, bit_out;
    mtype type = MESSAGE_A;
    bool loss;

    do
        :: unreliable_send(sender2receiver, type, bit_out) ->
sent:       receiver2sender?ACK(bit_in);
            if
                :: bit_in == bit_out ->
new_msg:            if
                        :: type = MESSAGE_A;
                        :: type = MESSAGE_B;
                    fi;
                    bit_out = 1 - bit_out;
                :: else ->
                    skip
            fi
    od
}

active proctype Receiver () {
    bit bit_in = 1;
    mtype type;

    do
        :: sender2receiver?type(bit_in);
        :: receiver2sender!ACK(bit_in);
    od
}


#define sentA (Sender@sent && Sender:type == MESSAGE_A)
#define sentB (Sender@sent && Sender:type == MESSAGE_B)
#define recA (Sender@new_msg && Receiver:type == MESSAGE_A)
#define recB (Sender@new_msg && Receiver:type == MESSAGE_B)
#define prec(a, b) (<>(b) -> (!(b) U (a)))
#define fairrun ([]<> (_last == 0) && []<> (_last == 1))

/* response to impulse */
ltl p1 { (fairrun && []<>!Sender:loss) -> [](sentA -> <>recA) }

/* absence of unsolicited response */
ltl p2 { (<> recA) -> ((!recA) U sentA) }

/* fifo */
ltl p3 { prec(sentA, sentB) -> prec(recA, recB) }

