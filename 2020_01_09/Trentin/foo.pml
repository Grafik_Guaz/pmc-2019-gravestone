bool b = true;

active proctype main() {
    printf("hello world!\n");
    b = false;
}

ltl p1 { [] b }

/**
 * verify that b is always true
 *
 * append:
 *     
 * $ spin -a foo.pml
 * $ gcc -o pan pan.c
 * $ ./pan -a -N p1
 */
