/**
 * Fifo Process
 * - push/pop failure "blocks":
 *   - `in` must be buffered: when a command can not be executed,
 *     we put it back inside `in` and move to the next one
 *     *without* sending an answer, so that the caller is effectively blocked
 * - it takes 51 seconds and 6GB of RAM to check that
 *   it contains no assertion violation with
 *   ~$ spin -search -bfs fifo_process_01.pml
 *
 */

mtype = { PUSH, POP, IS_EMPTY, IS_FULL, RETURN };

#define PRODUCER_UID 0
#define CONSUMER_UID 1
#define FIFO_SIZE    3

proctype fifo(chan in, out)
{
    byte data[FIFO_SIZE]; // internal storage
    byte head = 0;        // points at fifo head in data
    byte count = 0;       // number of elements in fifo

    mtype command;
    byte uid, value;

end:
    do
        :: in?command(uid, value) ->
            if
                :: command == PUSH ->
                    if
                        :: count >= FIFO_SIZE ->
                            in!command(uid, value);
                        :: else ->
write:
                            data[(head + count) % FIFO_SIZE] = value;
                            count = count + 1;
                            out!RETURN(uid, true, 0);
                    fi

                :: command == POP ->
                    if
                        :: count <= 0 ->
                            in!command(uid, value)
                        :: else ->
read:
                            out!RETURN(uid, true, data[head]);
                            head = (head + 1) % FIFO_SIZE;
                            count = count - 1;
                    fi

                :: command == IS_EMPTY ->
                    out!RETURN(uid, count <= 0, 0);

                :: command == IS_FULL ->
                    out!RETURN(uid, FIFO_SIZE <= count, 0);
            fi;
    od;
}

proctype producer(chan fifo_in, fifo_out)
{
    mtype command;
    bool res;
    byte value;
    byte cc;

    do
        :: cc < 10 ->
            select(value: 0..16);
            value = cc;
            printf("P[%d] - produced: %d\n", _pid, value);
try_push:
            atomic {
                fifo_in!PUSH(PRODUCER_UID, value) ->
                fifo_out?RETURN(PRODUCER_UID, res, 0);
            }
ok_push:
            assert(res);
            cc++;
        :: else -> break;
    od;
}

proctype consumer(chan fifo_in, fifo_out)
{
    mtype command;
    bool res;
    byte value;
    byte cc;

    do
        :: cc < 10 ->
try_pop:
            atomic {
                fifo_in!POP(CONSUMER_UID, 0) ->
                fifo_out?RETURN(CONSUMER_UID, res, value);
            }
ok_pop:
            assert(res);
            printf("P[%d] - consumed: %d\n", _pid, value);
            cc++;
        :: else -> break;
    od;
}

init {
    // NOTE: the size of `fifo_in` must be at least as large as
    // the number of processes who will eventually issue a command
    // on the FIFO, or deadlock might happen
    chan fifo_in  = [2] of { mtype, byte, byte };
    chan fifo_out = [0] of { mtype, byte, bool, byte };

    run fifo(fifo_in, fifo_out);     // pid: 1
    run producer(fifo_in, fifo_out); // pid: 2
    run consumer(fifo_in, fifo_out); // pid: 3
}

#define fairrun ([]<> (_last == 1) && []<> (_last == 2) && []<> (_last == 3))

// if the fifo is full, a write request is not served
// ltl p1 { ... }

// if the fifo is empty, a read request is not served
// ltl p2 { ... }

// the counter of fifo elements is always valid
// ltl p3 { ... }

// in a fair run, if the producer tries to push something on the fifo,
// it will eventually succeed
// ltl p4 { ... }

// in a fair run, if the consumer tries to pop something from the fifo,
// it will eventually succeed
// ltl p5 { ... }


/**
 * 1. What happens if the fair run requirement is dropped? (Why?)
 * 2. What happens if `cc < 10` is replaced with `true`? (Why?)
 */
