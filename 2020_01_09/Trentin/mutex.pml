/**
 * Peterson Mutual Exclusion for 2 processes
 */

bool turn;
bool flag[2];
byte cnt;

active [2] proctype mutex()
{
    pid i, j;

    i = _pid;
    j = 1 - _pid;

trying:
    flag[i] = true;
    turn = i;
    !(flag[j] && turn == i) ->

critical:
    cnt++; assert(cnt == 1); cnt--;

    flag[i] = false;
    goto trying
}

ltl p1 { []! (mutex[0]@critical && mutex[1]@critical) }
ltl p2 { [] ((mutex[0]@trying   || mutex[1]@trying  ) ->
         <>  (mutex[0]@critical || mutex[1]@critical))}
ltl p3 { /* ToDo */


/**
 * verify the following LTL properties:
 * - mutual exclusion
 * - progress
 * - lockout-freedom
 */
